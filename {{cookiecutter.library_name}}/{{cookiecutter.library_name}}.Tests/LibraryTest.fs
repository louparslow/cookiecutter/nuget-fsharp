namespace {{cookiecutter.library_name}}.Tests

open NUnit.Framework
open {{cookiecutter.library_name}}

[<TestFixture>]
type LibraryTest () =

    [<Test>]
    member this.Hello() =
        Library.hello "developer"

    [<Test>]
    member this.Add() =
        Assert.AreEqual(3,Library.add 1 2)
