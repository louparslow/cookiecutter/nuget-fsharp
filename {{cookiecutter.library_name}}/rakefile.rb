NAME="{{cookiecutter.library_name}}"
VERSION=IO.read("./#{NAME}/#{NAME}.fsproj").scan(/<Version>([\d.]+)<\/Version>/).last.first
require 'rake/clean'

puts "VERSION #{VERSION}"
CLEAN.include('**/bin','**/obj','build','test')

task :setup do
    puts `dotnet restore #{NAME}.sln`
end

task :build => [:setup] do
    puts `dotnet build --configuration Release`
end

task :test =>[:build] do
    puts `dotnet test --configuration Release`
end

task :publish => [:test] do
    FileUtils.cp("#{NAME}/bin/Release/#{NAME}.#{VERSION}.nupkg","#{NAME}.#{VERSION}.nupkg")
	puts 'pushing to AzureDevOps'
	puts `nuget.exe push -Source "VSTS" -ApiKey AzureDevOps #{NAME}.#{VERSION}.nupkg`
end

task :default => [:test]
