namespace {{cookiecutter.library_name}}

module Library =
    let hello name =
        printfn "Hello %s" name

    let add x y =
        x + y
